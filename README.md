# YnovFramework

Par : Florian AUBIN, Alexis Brohan, Axel Borget 

## Liens vers les gits : 

Framework : https://gitlab.com/florian.aubin974/ynovframework

CLI : https://gitlab.com/Axel44/ppftynov/

## Conditions d'utilisations du framework

 * Un fichier doit contenir un seul et strictement un seul filtre

 * Dans chaque filter (même le premier), dans l'input, le resultat du précédent filter est en place 0 (exemple : input[0])

 * Si c'est le premier filtre alors input[0] sera égal à : ""

 * Il est necessaire d'avoir vérifié entièrement le fichier de config pour pouvoir continuer

   - Il faut ensuite vérifier si les fichiers sont conforme (une fonction, un return, ...)

   - Seulement alors il sera possible d'utiliser la fonction executeFiltersInOrder() avec en paramètre le premier filtre à passer



## Documentation CLI : 

(documentation CLI sur le dit CLI)
