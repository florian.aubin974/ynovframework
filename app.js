let file = require('file-system');
const fs = file.fs
const config = require('./config-filters.json')

// Fonction lancée eu démarrage du service
const start = () => {
    console.log("lancement de app.js");
    if (verifConfigFilters()) {
        if (verifFiles()) {
            executeFiltersInOrder(1);
        }
    }
}

//itere sur l'ensemble des fichiers (dans filters) et renvoie true si conforme (sinon false)
function verifFiles() {
    let booleanRet = true;
    for (const [key, value] of Object.entries(config.steps)) {
        let pathToVerify = "filters/"+value.filter + ".js";
        if (file.fs.existsSync(pathToVerify)){
            console.log('\x1b[32m',"Le filtre " + value.filter + " existe bien ! :)")
            console.log('\033[0m')
            booleanRet = verifFunction(pathToVerify)
        } else {
            console.error('\x1b[31m',"Le filtre " + value.filter + " n'existe pas ! :(")
            console.log('\033[0m')
            booleanRet = false;
        }
    }

    return booleanRet;
}

//verifie que le filtre retourne une fonction (valide ? )
function verifFunction(pathToVerify) {
    var element = require('./' + pathToVerify)
    if(typeof element !== 'function') {
        console.error('\x1b[31m',`Fichier ${pathToVerify} n'exporte pas de fonction`)
        console.log('\033[0m')
        return false
    }
    console.log('\x1b[32m',`Fichier ${pathToVerify} exporte bien une fonction`)
    console.log('\033[0m')
    return true
}

// vérifie la forme ET le bon fonctionnement du fichier config-filters.json
function verifConfigFilters() {
    let verifConfig = true

    if (!fs.existsSync('config-filters.json')) {
        console.error('Fichier config-filters.json non présent à la racine')
        return false
    }

    fs.readFile('config-filters.json', 'utf8' , (err, data) => {
        if (err) {
            console.error(err)
            verifConfig = false
            return;
        }

        const content = JSON.parse(data)

        const steps = content.steps
        if(!steps) {
            console.error('steps est requis')
            verifConfig = false
            return
        }

        for (const stepNumber in steps) {
            const step = steps[stepNumber]
            const filter = step.filter
            const params = step.params
            const next = step.next

            // Filter
            if(!filter) {
                console.error('filter est requis pour le step ' + stepNumber)
                verifConfig = false
                return
            } else if(typeof filter !== 'string') {
                console.error('filter doit être une string pour le step ' + stepNumber)
                verifConfig = false
                return
            }

            // Params
            if(params && !Array.isArray(params)) {
                console.error('params doit être un tableau pour le step ' + stepNumber)
                verifConfig = false
                return
            }

            // Next
            if(next) {
                if(typeof next !== 'string') {
                    console.error('next doit être une string pour le step ' + stepNumber)
                    verifConfig = false
                    return
                }

                if(!steps[next]) {
                    console.error('next est invalide pour le step ' + stepNumber)
                    verifConfig = false
                    return
                }
            }
        }
    })
    console.log('Fichier config-filters.json correct')
    return verifConfig
}

function executeFunction(filterNumber, stringPrev){
    let hasNext; //possède un next ?
    let nextNext; // numéro du next
    if ("next" in config.steps[filterNumber] == 1){
        hasNext = true;
        nextNext = config.steps[filterNumber].next;
    } else {
        hasNext = false;
        nextNext = null;
    }
    if (stringPrev == undefined){
        stringPrev = "";
    }
    let inputs = [];
    inputs[0] = stringPrev;
    for (const [key, value] of Object.entries(config.steps[filterNumber].params)) {
        inputs.push(value);
    }

    let filter = require('./filters/'+config.steps[filterNumber].filter+'.js');
    let inputNext = filter(inputs)

    return [hasNext, nextNext, inputNext];
}

function executeFiltersInOrder(firstFilterNumber){
    printFilterNumber(firstFilterNumber);
    let again = executeFunction(firstFilterNumber);
    while (again[0]){
        printFilterNumber(again[1]);
        again = executeFunction(again[1], again[2]);
    }
}

function printFilterNumber(positionOfFilters){
    console.log("Le filtre en cours est le : " + positionOfFilters);
}

start();

